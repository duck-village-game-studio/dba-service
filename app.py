from flask import Flask, request
import pymongo
import os

app = Flask(__name__)


def get_table_from_db(dbname, table_name):
    mongo_host = os.getenv('MONGO_HOST')
    mongo_port = os.getenv('MONGO_PORT')
    mongo_username = os.getenv('MONGO_USERNAME')
    mongo_password = os.getenv('MONGO_PASSWORD')
    client = pymongo.MongoClient(f'mongodb://{mongo_username}:{mongo_password}@{mongo_host}:{mongo_port}/')
    mydb = client[dbname]
    mycol = mydb[table_name]
    return mycol


@app.route('/')
def hello():
    return "Hello World!"


@app.route('/create_village_for_user', methods=['POST'])
def create_village():
    data = request.json
    if request.method == 'POST':
        username = data['username']
        village_name = data['villageName']
    else:
        return "Not get method"
    mycol = get_table_from_db("DuckVIllage", "villages")
    village_dict = {"user": username,
                    "stone": 10,
                    "wood": 10,
                    "villageName": village_name,
                    "townHall": 1,
                    "walls": 0,
                    'warehouse': 1,
                    'stonemason': 1,
                    'lumberjack': 1, }
    x = mycol.insert_one(village_dict)
    return "Hello create_village_for_user!"


@app.route('/get_all_villages')
def get_all_villages():
    mycol = get_table_from_db("DuckVIllage", "villages")
    return str([village for village in mycol.find()])


@app.route('/get_village_for_user', methods=['POST'])
def get_village():
    data = request.json
    if request.method == 'POST':
        username = data['username']
    else:
        return "Not get method"
    mycol = get_table_from_db("DuckVIllage", "villages")
    return str([village for village in mycol.find({"user": username})])


@app.route('/upgrade_building', methods=['POST'])
def upgrade_building():
    UPGRADE_MULTIPLIER = 10
    FORBIDDEN = 403
    NOT_ACCEPTABLE = 403
    MAX_LEVEL = 10
    data = request.json
    if request.method == 'POST':
        village_name = data['villageName']
        building_id = data['buildingId']
    else:
        return "Not get method"
    mycol = get_table_from_db("DuckVIllage", "villages")

    villages = mycol.find({"villageName": village_name})
    for village in villages:
        if village[building_id] == MAX_LEVEL:
            return 'upgrade impossible', FORBIDDEN
        upgrade_requirement = (village[building_id] + 1) * UPGRADE_MULTIPLIER
        if village['wood'] >= upgrade_requirement and village['stone'] >= upgrade_requirement:
            new_data = {"stone": village["stone"] - upgrade_requirement,
                        "wood": village["wood"] - upgrade_requirement,
                        building_id: village[building_id] + 1}
            newvalues = {"$set": new_data}
            mycol.update_one({"villageName": village["villageName"]}, newvalues)
            return 'Upgrade performed!'
        else:
            return 'not enough resources!', NOT_ACCEPTABLE


@app.route('/add_resources')
def add_resources():
    mycol = get_table_from_db("DuckVIllage", "villages")
    villages = mycol.find({})
    for village in villages:
        new_data = {"stone": village["stone"] + village["stonemason"],
                    "wood": village["wood"] + village["lumberjack"]}
        newvalues = {"$set": new_data}
        mycol.update_one({"villageName": village["villageName"]}, newvalues)
    return "Resource added"


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
