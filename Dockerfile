FROM python:3.7.5-alpine
COPY . /app
WORKDIR /app
ENV PORT=8080
RUN pip install -r requirements.txt
CMD python app.py
