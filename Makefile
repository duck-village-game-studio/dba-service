DOCKER=docker
DOCKER_IMAGE_NAME=registry.gitlab.com/duck-village-game-studio/dba-service
PORT=5000
SERVICE_NAME=dba-service

start:
	${DOCKER} build -t ${SERVICE_NAME} .
	${DOCKER} run -p ${PORT}:${PORT} -it ${SERVICE_NAME} bash

build:
	${DOCKER} build . -t ${DOCKER_IMAGE_NAME}

run:
	${DOCKER} run -p ${PORT}:${PORT} -e PORT=${PORT} ${DOCKER_IMAGE_NAME}

push:
	${DOCKER} push ${DOCKER_IMAGE_NAME}
start-compose:
	@echo "Running docker-compose server"
	docker-compose up --build
