from pymongo import MongoClient
import os
mongo_host = os.getenv('MONGO_HOST')
mongo_port = os.getenv('MONGO_PORT')
mongo_username = os.getenv('MONGO_USERNAME')
mongo_password = os.getenv('MONGO_PASSWORD')
client = MongoClient(f'mongodb://{mongo_username}:{mongo_password}@{mongo_host}:{mongo_port}/')
post = {"test": 123}
db = client.test_database
posts = db.posts
post_id = posts.insert_one(post).inserted_id
print(f'mongodb://{mongo_username}:{mongo_password}@{mongo_host}:{mongo_port}/')